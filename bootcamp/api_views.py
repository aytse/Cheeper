import json
import base64
import random
import requests
import os
import time

from django.conf import settings as django_settings
from django.contrib.auth.models import User
from django.http import HttpRequest
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from authentication.models import Profile
from feeds.models import Feed
from messenger.models import Message
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from rest_framework.parsers import MultiPartParser
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from serializers import FeedSerializerGet
from serializers import FeedSerializerPost
from serializers import MessageSerializer
from serializers import ProfileSerializer
from process_request import Request

from PIL import Image
import shutil

from datetime import datetime, timedelta

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

# NAME:         Get Client IP
# PARAMETERS:   Takes url django request as parameter
# RETURN:       Returns string of user's ip
# DESCRIPTION:  Identifies user IP by extracting it from request.  If the IP is 127.0.0.1, it is identified as localhost
#                   Used to verify, if user is allowed_host for making requests to the API.
def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')

    if ip == '127.0.0.1':
        ip = 'localhost'

    return ip

# NAME:         Api Feed
# PARAMETERS:   For get requests, url parameters for last number of posts, the type of post (profile or group)
#                   and the source of the post (user id or group name); for post requests, it takes json data
# RETURN:       Returns json of either query result or success messages for post requests
# DESCRIPTION:  Method is for all Feed interaction using the API.  You can query feeds by the user or group post
#                   how many are needed.  Additionally, you can post your own messages to feed by supplying from which
#                   user you want to send the post, the post content, and the group you want it to be sent or all.
#                   Additionally, you can send images by posting an image id.  You must be one of the host IPs to be
#                   able to call this method.
@api_view(['GET','POST'])
@csrf_exempt
def api_feed(request, number=None, type=None, source=None):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    #if request.user.username == 'admin':
    if request.method == 'POST':
        data = JSONParser().parse(request)

        r = Request(data)
        image = "none"

        if data["user"] != request.user.id and request.user.username != 'admin':
            return JSONResponse('Unauthorized user', status=400)
        else:
            if 'image' in data:
                image = data["image"]

            if not r.valid_user(data["user"]):
                return JSONResponse("The user does not exist", status=400)
            if not r.format_group(data["group"]):
                return JSONResponse("The group does not exist", status=400)
            data = r.post_feed()

            serializer = FeedSerializerPost(data=data)

            if serializer.is_valid():
                serializer.save()

                if image is not 'none':
                    try:
                        for file in os.listdir(django_settings.POST_IMAGES):
                            if image in file:
                                image = file
                                break

                        imagefile = django_settings.POST_IMAGES + "/" + image
                        extension = os.path.splitext(image)[1]

                        post_pictures = django_settings.MEDIA_ROOT + '/post_pictures/'
                        if not os.path.exists(post_pictures):
                            os.makedirs(post_pictures)
                        filename = django_settings.MEDIA_ROOT + '/post_pictures/' + \
                                   str(serializer.data["id"]) + extension

                        shutil.copyfile(imagefile, filename)

                        # im = Image.open(imagefile)

                        # im.save(filename, 'png')
                        # width, height = im.size
                        # if width > 350:
                        #    new_width = 350
                        #    new_height = (height * 350) / width
                        #    new_size = new_width, new_height
                        #    im.thumbnail(new_size, Image.ANTIALIAS)
                        #    im.save(filename, 'png')
                    except Exception as e:
                        print e

                return JSONResponse(serializer.data, status=201)
            else:
                return JSONResponse(serializer.errors, status=400)


    elif request.method == 'GET':
        if type == 'group':
            feeds = Feed.objects.all().filter(group=source)
        elif type == 'profile':
            feeds = Feed.objects.all().filter(group='none')
            feeds = feeds.filter(user__id=source)
        else:
            feeds = Feed.objects.all()

        feeds = feeds.order_by('-id')
        if number is not None:
            feeds = feeds[:number]

        serializer = FeedSerializerGet(list(feeds.values()), many=True)

        return JSONResponse(serializer.data, status=201)

    #else:
    #    return JSONResponse('Unauthorized user', status=400)


# NAME:         Api Message
# PARAMETERS:   For get requests, url parameters for last number of messages, the source of the message(user id), and
#                   the receiver of the message(user_id); for post requests, it takes json data
# RETURN:       Returns json of either query result or success messages for post requests
# DESCRIPTION:  Method is for all Message interaction using the API.  You can query messages by the sender user,
#                   receiving user, and how many are needed.  Additionally, you can post your own messages by supplying
#                   from which user you want to send the message, the message content, and the user you want it to be
#                   sent to.  Additionally, you can send images by posting an image id.  You must be one of the host IPs
#                   to be able to call this method.
@api_view(['GET','POST'])
@csrf_exempt
def api_message(request, number=None, to_user=None, from_user=None):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    if request.user.username == 'admin':

        if request.method == 'POST':
            data = JSONParser().parse(request)
            r = Request(data)
            image = "none"

            if 'image' in data:
                image = data["image"]

            if not r.valid_user(data["user"]) or not r.valid_user(data["from_user"]):
                return JSONResponse("The user does not exist", status=400)

            data = r.post_feed()

            serializer = MessageSerializer(data=data)

            if serializer.is_valid():
                serializer.save()

                if image is not 'none':
                    try:
                        for file in os.listdir(django_settings.POST_IMAGES):
                            if image in file:
                                image = file
                                break

                        imagefile = django_settings.POST_IMAGES + "/" + image
                        extension = os.path.splitext(image)[1]

                        post_pictures = django_settings.MEDIA_ROOT + '/message_pictures/'
                        if not os.path.exists(post_pictures):
                            os.makedirs(post_pictures)
                        filename = django_settings.MEDIA_ROOT + '/message_pictures/' + \
                                   str(serializer.data["id"]) + extension

                        shutil.copyfile(imagefile, filename)

                        # im = Image.open(imagefile)

                        # im.save(filename, 'png')
                        # width, height = im.size
                        # if width > 350:
                        #    new_width = 350
                        #    new_height = (height * 350) / width
                        #    new_size = new_width, new_height
                        #    im.thumbnail(new_size, Image.ANTIALIAS)
                        #    im.save(filename, 'png')
                    except Exception as e:
                        print e

                data["user"] = data["conversation"]
                data["conversation"] = data["from_user"]
                serializer = MessageSerializer(data=data)

                if serializer.is_valid():
                    serializer.save()

                    if image is not 'none':
                        try:
                            imagefile = django_settings.POST_IMAGES + "/" + image
                            extension = os.path.splitext(image)[1]
                            filename = django_settings.MEDIA_ROOT + '/message_pictures/' + \
                                       str(serializer.data["id"]) + extension

                            shutil.copyfile(imagefile, filename)

                            #im = Image.open(imagefile)

                            #im.save(filename, 'png')
                            #width, height = im.size
                            #if width > 350:
                            #    new_width = 350
                            #    new_height = (height * 350) / width
                            #    new_size = new_width, new_height
                            #    im.thumbnail(new_size, Image.ANTIALIAS)
                            #    im.save(filename, 'png')
                        except Exception as e:
                            print e

                    return JSONResponse(serializer.data, status=201)
                else:
                    return JSONResponse(serializer.errors, status=400)
            else:
                return JSONResponse(serializer.errors, status=400)

        elif request.method == 'GET':
            if to_user is None:
                messages = Message.objects.all()
            else:
                messages = Message.objects.all().filter(user=to_user)
                if from_user is not None:
                    messages = messages.filter(from_user=from_user)

            messages = messages.order_by('-id')
            if number is not None:
                messages = messages[:number]

            serializer = MessageSerializer(messages, many=True)

            return JSONResponse(serializer.data, status=201)
    else:
        return JSONResponse('Unauthorized user', status=400)

@api_view(['GET'])
@csrf_exempt
def api_delete_old(request):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    if request.user.username == 'admin':
        if request.method == 'GET':
            feed_time_threshold = datetime.now() - timedelta(minutes=30)
            message_time_threshold = datetime.now() - timedelta(hours=1)
            feeds = Feed.objects.all().filter(date__lt=feed_time_threshold)
            messages = Message.objects.all().filter(date__lt=message_time_threshold)

            for to_delete in feeds:
                for filename in os.listdir(django_settings.MEDIA_ROOT + '/post_pictures/'):
                    if str(to_delete.id) in filename:
                        try:
                            os.remove(django_settings.MEDIA_ROOT + '/post_pictures/' + filename)
                        except:
                            return JSONResponse({'status': 'ERROR DELETING POST PICTURE - %s' % filename}, status=201)

                likes = to_delete.get_likes()
                parent = to_delete.parent
                for like in likes:
                    like.delete()
                to_delete.delete()
                if parent:
                    parent.calculate_comments()

            for to_delete in messages:
                for filename in os.listdir(django_settings.MEDIA_ROOT + '/message_pictures/'):
                    if str(to_delete.id) in filename:
                        try:
                            os.remove(django_settings.MEDIA_ROOT + '/message_pictures/' + filename)
                        except:
                            return JSONResponse({'status': 'ERROR DELETING MESSAGE PICTURE - %s' % filename}, status=201)

                to_delete.delete()

            return JSONResponse({'status': 'SUCCESS'}, status=201)
    else:
        return JSONResponse({'status': 'Unauthorized user'}, status=400)


@api_view(['GET'])
@csrf_exempt
def api_delete_all(request):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    if request.user.username == 'admin':
        if request.method == 'GET':
            feeds = Feed.objects.all()
            messages = Message.objects.all()

            for to_delete in feeds:
                for filename in os.listdir(django_settings.MEDIA_ROOT + '/post_pictures/'):
                    if str(to_delete.id) in filename:
                        try:
                            os.remove(django_settings.MEDIA_ROOT + '/post_pictures/' + filename)
                        except:
                            return JSONResponse({'status': 'ERROR DELETING POST PICTURE - %s' % filename}, status=201)

                likes = to_delete.get_likes()
                parent = to_delete.parent
                for like in likes:
                    like.delete()
                to_delete.delete()
                if parent:
                    parent.calculate_comments()

            for to_delete in messages:
                for filename in os.listdir(django_settings.MEDIA_ROOT + '/message_pictures/'):
                    if str(to_delete.id) in filename:
                        try:
                            os.remove(django_settings.MEDIA_ROOT + '/message_pictures/' + filename)
                        except:
                            return JSONResponse({'status': 'ERROR DELETING MESSAGE PICTURE - %s' % filename}, status=201)

                to_delete.delete()

            return JSONResponse({'status': 'SUCCESS'}, status=201)
    else:
        return JSONResponse({'status': 'Unauthorized user'}, status=400)


@api_view(['POST'])
@csrf_exempt
def api_add_user(request):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    if request.user.username == 'admin':
        data = request.POST

        profile_data = {'username': data['username'], 'email': data['email'], 'password': data['password']}

        user = User.objects.create_user(username=profile_data['username'], email=profile_data['email'],
                                        password=profile_data['password'])

        if 'location' in data or 'job_title' in data or 'url' in data:
            profile = Profile.objects.all().filter(user=user)[0]
            if 'location' in data:
                profile.location = data['location']
            if 'job_title' in data:
                profile.job_title = data['job_title']
            if 'url' in data:
                profile.url = data['url']
            profile.save()


        if request.FILES:
            ext = request.FILES['profile_picture'].name.split('.')[1]
            image = django_settings.MEDIA_ROOT + '/profile_pictures/' + data['username']
            imagefile = django_settings.MEDIA_ROOT + '/profile_pictures/' + data['username'] + '.' + ext

            try:
                with open(imagefile, 'wb+') as destination:
                    for chunk in request.FILES['profile_picture'].chunks():
                        destination.write(chunk)

                im = Image.open(imagefile)

                new_size = 200, 200
                im.thumbnail(new_size, Image.ANTIALIAS)
                if ext != 'jpg':
                    im = im.convert('RGB')
                im.save(image + '.jpg')

                if ext != 'jpg':
                    os.remove(imagefile)

            except Exception as e:
                print e

        return JSONResponse({'status': 'SUCCESS'}, status=201)

    else:
        return JSONResponse({'status': 'Unauthorized user'}, status=400)


@api_view(['GET'])
@csrf_exempt
def api_event(request, event_id=None):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    if request.user.username == 'admin':
        if request.method == 'GET':
            with open(django_settings.STATIC_ROOT + '/res/MC3MSEL', 'r') as json_file:
                mc2msel = json.load(json_file)
            if event_id in mc2msel:
                posts = mc2msel[event_id]['posts']
                for post in posts:
                    delay = random.randrange(85, 115)
                    time.sleep(delay)

                    user = random.randrange(2, 987)
                    group = 'none'
                    post = post.replace("\\'", "\'")
                    post_fields = {'user': user, 'post': post, 'group': group}  # Set POST fields here
                    headers = {'Authorization': 'Basic %s' % base64.b64encode('admin:Rx846nTw')}

                    requests.post('http://cheeper.com/api/feed/', data=json.dumps(post_fields), auth=('admin', 'Rx846nTw'))

                return JSONResponse({'status': 'SUCCESS'}, status=201)

            else:
                return JSONResponse({'status': 'Event does not exist'}, status=400)
    else:
        return JSONResponse({'status': 'Unauthorized user'}, status=400)
