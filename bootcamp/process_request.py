import datetime

from authentication.models import Profile
from feeds.models import Feed
from messenger.models import Message

class Request:
    def __init__(self, obj):
        self.obj = obj
        self.index = 0

    def post_feed(self):
        self.obj['parent_id'] = None

        if ('image' in self.obj):
            self.obj.pop('image', None)

        return self.obj


    def post_message(self):

        if ('image' in self.obj):
            self.obj.pop('image', None)
        return self.obj


    @staticmethod
    def valid_user(user_id):
        buf = Profile.objects.all()
        for i in buf:
            if i.user.id == user_id:
                return True
        return False


    @staticmethod
    def format_group(group_name):
        group_name = group_name.lower()
        if group_name == 'argentina':
            return 'Argentina'
        elif group_name == 'france':
            return 'France'
        elif group_name == 'italy':
            return 'Italy'
        elif group_name == 'mexico':
            return 'Mexico'
        elif group_name == 'morocco':
            return 'Morocco'
        elif group_name == 'south africa':
            return 'South Africa'
        elif group_name == 'sri lanka':
            return 'Sri Lanka'
        elif group_name == 'suriname':
            return 'Suriname'
        elif group_name == 'none':
            return 'none'
        else:
            return None
