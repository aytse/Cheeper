import json
import os

from django.conf import settings as django_settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import redirect, render

from bootcamp.decorators import ajax_required
from bootcamp.messenger.models import Message

from PIL import Image

@login_required
def inbox(request):
    conversations = Message.get_conversations(user=request.user)
    active_conversation = None
    messages = None
    if conversations:
        conversation = conversations[0]
        active_conversation = conversation['user'].username
        messages = Message.objects.filter(user=request.user,
                                          conversation=conversation['user'])
        messages.update(is_read=True)
        for conversation in conversations:
            if conversation['user'].username == active_conversation:
                conversation['unread'] = 0

    return render(request, 'messenger/inbox.html', {
        'messages': messages,
        'conversations': conversations,
        'active': active_conversation
        })


@login_required
def messages(request, username):
    conversations = Message.get_conversations(user=request.user)
    active_conversation = username
    messages = Message.objects.filter(user=request.user,
                                      conversation__username=username)
    messages.update(is_read=True)
    for conversation in conversations:
        if conversation['user'].username == username:
            conversation['unread'] = 0

    return render(request, 'messenger/inbox.html', {
        'messages': messages,
        'conversations': conversations,
        'active': active_conversation
        })


@login_required
def new(request):
    if request.method == 'POST':
        from_user = request.user
        to_user_username = request.POST.get('to')
        try:
            to_user = User.objects.get(username=to_user_username)

        except Exception:
            try:
                to_user_username = to_user_username[
                    to_user_username.rfind('(')+1:len(to_user_username)-1]
                to_user = User.objects.get(username=to_user_username)

            except Exception:
                return redirect('/messages/new/')

        message = request.POST.get('message')

        image = 'none'
        if request.FILES:
            image = request.FILES['picture']

        if len(message.strip()) == 0 and image == 'none':
            return redirect('/messages/new/')

        if from_user != to_user:
            msg = Message.send_message(from_user, to_user, message)

            if image is not 'none':
                try:
                    post_pictures = django_settings.MEDIA_ROOT + '/message_pictures/'
                    if not os.path.exists(post_pictures):
                        os.makedirs(post_pictures)
                    filename = django_settings.MEDIA_ROOT + '/message_pictures/' + \
                               str(msg.id) + '.png'
                    with open(filename, 'wb+') as destination:
                        for chunk in image.chunks():
                            destination.write(chunk)
                    filename = django_settings.MEDIA_ROOT + '/message_pictures/' + \
                               str(msg.id+1) + '.png'
                    with open(filename, 'wb+') as destination:
                        for chunk in image.chunks():
                            destination.write(chunk)
                    #im = Image.open(filename)
                    #width, height = im.size
                    #if width > 350:
                    #    new_width = 350
                    #    new_height = (height * 350) / width
                    #    new_size = new_width, new_height
                    #    im.thumbnail(new_size, Image.ANTIALIAS)
                    #    im.save(filename, 'png')
                except Exception as e:
                    print e

        return redirect('/messages/{0}/'.format(to_user_username))

    else:
        conversations = Message.get_conversations(user=request.user)
        return render(request, 'messenger/new.html',
                      {'conversations': conversations})


@login_required
@ajax_required
def delete(request):
    return HttpResponse()


@login_required
@ajax_required
def send(request):
    if request.method == 'POST':
        from_user = request.user
        to_user_username = request.POST.get('to')
        to_user = User.objects.get(username=to_user_username)
        message = request.POST.get('message')

        image = 'none'
        if request.FILES:
            image = request.FILES['picture']

        if len(message.strip()) == 0 and image is 'none':
            return HttpResponse()

        if from_user != to_user:
            msg = Message.send_message(from_user, to_user, message)

            if image is not 'none':
                try:
                    post_pictures = django_settings.MEDIA_ROOT + '/message_pictures/'
                    if not os.path.exists(post_pictures):
                        os.makedirs(post_pictures)
                    filename = django_settings.MEDIA_ROOT + '/message_pictures/' + \
                               str(msg.id) + '.png'
                    with open(filename, 'wb+') as destination:
                        for chunk in image.chunks():
                            destination.write(chunk)
                    filename = django_settings.MEDIA_ROOT + '/message_pictures/' + \
                               str(msg.id+1) + '.png'
                    with open(filename, 'wb+') as destination:
                        for chunk in image.chunks():
                            destination.write(chunk)
                    #im = Image.open(filename)
                    #width, height = im.size
                    #if width > 350:
                    #   new_width = 350
                    #    new_height = (height * 350) / width
                    #    new_size = new_width, new_height
                    #    im.thumbnail(new_size, Image.ANTIALIAS)
                    #    im.save(filename, 'png')
                except Exception as e:
                    print e

            return render(request, 'messenger/includes/partial_message.html',
                          {'message': msg})

        return HttpResponse()
    else:
        return HttpResponseBadRequest()


@login_required
@ajax_required
def users(request):
    users = User.objects.filter(is_active=True)
    dump = []
    template = '{0} ({1})'
    for user in users:
        if user.profile.get_screen_name() != user.username:
            dump.append(template.format(user.profile.get_screen_name(),
                                        user.username))
        else:
            dump.append(user.username)
    data = json.dumps(dump)
    return HttpResponse(data, content_type='application/json')


@login_required
@ajax_required
def check(request):
    count = Message.objects.filter(user=request.user, is_read=False).count()
    return HttpResponse(count)
