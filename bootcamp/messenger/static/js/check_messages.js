$("#btn-upload-picture").click(function () {
  $("#imageInput").click();
});

$("#imageInput").change(function () {
    var reader = new FileReader();
    reader.onload = function (e) {
      var upload_image = "<img width=\"350\" src=\"" + e.target.result + "\" class=\"post-image\">";
      $(".post-image").remove();
      $(".message-image-temp").append(upload_image);
    };
    reader.readAsDataURL(this.files[0]);
});

$(function () {
  function check_messages() {
    $.ajax({
      url: '/messages/check/',
      cache: false,
      success: function (data) {
        $("#unread-count").text(data);
      },
      complete: function () {
        window.setTimeout(check_messages, 60000);
      }
    });
  };
  check_messages();
});