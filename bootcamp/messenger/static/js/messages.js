$(function () {
  $("#send").submit(function () {

    var formData = new FormData($("#send")[0]);

    $.ajax({
      url: '/messages/send/',
      data: formData,
      cache: false,
      type: 'post',
      processData: false,
      contentType: false,
      success: function (data) {
        $(".post-image").remove();
        $(".send-message").before(data);
        $("input[name='message']").val('');
        $("input[name='message']").focus();
      }
    });
    return false;
  });
});