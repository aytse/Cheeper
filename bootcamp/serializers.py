from feeds.models import Feed
from messenger.models import Message
from authentication.models import Profile

from django.contrib.auth.models import User
from rest_framework import serializers

class FeedSerializerGet(serializers.ModelSerializer):
    class Meta:
        model = Feed
        fields = [
            'id', 'user_id', 'comments', 'parent_id', 'likes', 'date', 'group', 'post'
        ]

class FeedSerializerPost(serializers.ModelSerializer):
    class Meta:
        model = Feed
        fields = [
            'id', 'user', 'comments', 'parent_id', 'likes', 'date', 'group', 'post'
        ]

    post = serializers.CharField(allow_blank=True, max_length=255)

class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = [
            'id', 'user', 'message', 'date', 'conversation', 'from_user', 'is_read'
        ]

    message = serializers.CharField(allow_blank= True, max_length=1000)


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = [
            'location', 'url', 'job_title'
        ]


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'username', 'password', 'email'
        ]

    def create(self, validated_data):
        user = User.objects.create_user(username=validated_data['username'], email=validated_data['email'], password=validated_data['password'])

        profile_data = validated_data.pop('profile')

        profile = Profile.objects.create(user=user, location=profile_data['location'], url=profile_data['url'], job_title=profile_data['job_title'])

        return user
