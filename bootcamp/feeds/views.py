import json
import os

from django.conf import settings as django_settings
from django.contrib.auth.decorators import login_required
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.http import (HttpResponse, HttpResponseBadRequest,
                         HttpResponseForbidden)
from django.shortcuts import get_object_or_404, render
from django.template.context_processors import csrf
from django.template.loader import render_to_string

from bootcamp.activities.models import Activity
from bootcamp.decorators import ajax_required
from bootcamp.feeds.models import Feed

from PIL import Image

FEEDS_NUM_PAGES = 10


def feeds(request):
    all_feeds = Feed.get_feeds()
    all_feeds = all_feeds.filter(group='none')
    paginator = Paginator(all_feeds, FEEDS_NUM_PAGES)
    feeds = paginator.page(1)
    from_feed = -1
    if feeds:
        from_feed = feeds[0].id
    return render(request, 'feeds/feeds.html', {
        'feeds': feeds,
        'from_feed': from_feed,
        'page': 1,
        })


def feed(request, pk):
    feed = get_object_or_404(Feed, pk=pk)
    return render(request, 'feeds/feed.html', {'feed': feed})


def load(request):
    from_feed = request.GET.get('from_feed')
    page = request.GET.get('page')
    feed_source = request.GET.get('feed_source')
    feed_type = request.GET.get('feed_type')
    all_feeds = Feed.get_feeds(from_feed)

    if feed_source != 'all':
        if feed_type == 'profile':
            all_feeds = all_feeds.filter(user__id=feed_source)
            all_feeds = all_feeds.filter(group='none')
        elif feed_type == 'group':
            all_feeds = all_feeds.filter(group=feed_source)
    else:
        all_feeds = all_feeds.filter(group='none')
    paginator = Paginator(all_feeds, FEEDS_NUM_PAGES)
    try:
        feeds = paginator.page(page)
    except PageNotAnInteger:
        return HttpResponseBadRequest()
    except EmptyPage:
        feeds = []
    html = ''

    csrf_token = (csrf(request)['csrf_token'])

    for feed in feeds:
        html = '{0}{1}'.format(html,
                               render_to_string('feeds/partial_feed.html',
                                                {
                                                    'feed': feed,
                                                    'user': request.user,
                                                    'csrf_token': csrf_token
                                                    }).encode('utf-8'))

    return HttpResponse(html)


def _html_feeds(last_feed, user, csrf_token, feed_type, feed_source):
    feeds = Feed.get_feeds_after(last_feed)
    if feed_source != 'all':
        if feed_type == 'profile':
            feeds = feeds.filter(user__id=feed_source)
            feeds = feeds.filter(group='none')
        elif feed_type == 'group':
            feeds = feeds.filter(group=feed_source)
    else:
        feeds = feeds.filter(group='none')
    html = ''

    for feed in feeds:
        html = '{0}{1}'.format(html,
                               render_to_string('feeds/partial_feed.html',
                                                {
                                                    'feed': feed,
                                                    'user': user,
                                                    'csrf_token': csrf_token
                                                    }).encode('utf-8'))

    return html


def load_new(request):
    user = request.user
    last_feed = request.GET.get('last_feed')
    feed_source = request.GET.get('feed_source')
    feed_type = request.GET.get('feed_type')
    csrf_token = (csrf(request)['csrf_token'])

    html = _html_feeds(last_feed, user, csrf_token, feed_type, feed_source)
    return HttpResponse(html)


def check(request):
    last_feed = request.GET.get('last_feed')
    feed_source = request.GET.get('feed_source')
    feed_type = request.GET.get('feed_type')
    feeds = Feed.get_feeds_after(last_feed)

    if feed_source != 'all':
        if feed_type == 'profile':
            feeds = feeds.filter(user__id=feed_source)
            feeds = feeds.filter(group='none')
        elif feed_type == 'group':
            feeds = feeds.filter(group=feed_source)
    else:
        feeds = feeds.filter(group='none')
    count = feeds.count()
    return HttpResponse(count)

@login_required
@ajax_required
def post(request):
    last_feed = request.POST.get('last_feed')
    user = request.user
    csrf_token = (csrf(request)['csrf_token'])
    feed = Feed()
    feed.user = user

    image = 'none'
    if request.FILES:
        image = request.FILES['picture']

    post = request.POST['post']
    post = post.strip()
    if len(post) > 0 or image is not 'none':
        feed.post = post[:255]
        feed.save()

    if image is not 'none':
        try:
            post_pictures = django_settings.MEDIA_ROOT + '/post_pictures/'
            if not os.path.exists(post_pictures):
                os.makedirs(post_pictures)
            filename = django_settings.MEDIA_ROOT + '/post_pictures/' +\
                str(feed.id) + '.png'
            with open(filename, 'wb+') as destination:
                for chunk in image.chunks():
                    destination.write(chunk)
            im = Image.open(filename)
            width, height = im.size
            if width > 350:
                new_width = 350
                new_height = (height * 350) / width
                new_size = new_width, new_height
                im.thumbnail(new_size, Image.ANTIALIAS)
                im.save(filename, 'png')
        except Exception as e:
            print e

    html = _html_feeds(last_feed, user, csrf_token, 'profile', 'all')
    return HttpResponse(html)


@login_required
@ajax_required
def group_post(request):
    last_feed = request.POST.get('last_feed')
    user = request.user
    csrf_token = (csrf(request)['csrf_token'])
    feed = Feed()
    feed.user = user

    image = 'none'
    if request.FILES:
        image = request.FILES['picture']

    post = request.POST['post']
    post = post.strip()
    if len(post) > 0 or image is not 'none':
        feed.group = request.user.groups.values_list('name', flat=True)[0]
        feed.post = post[:255]
        feed.save()

    if image is not 'none':
        try:
            post_pictures = django_settings.MEDIA_ROOT + '/post_pictures/'
            if not os.path.exists(post_pictures):
                os.makedirs(post_pictures)
            filename = django_settings.MEDIA_ROOT + '/post_pictures/' +\
                str(feed.id) + '.png'
            with open(filename, 'wb+') as destination:
                for chunk in image.chunks():
                    destination.write(chunk)
            #im = Image.open(filename)
            #width, height = im.size
            #if width > 350:
            #    new_width = 350
            #    new_height = (height * 350) / width
            #    new_size = new_width, new_height
            #    im.thumbnail(new_size, Image.ANTIALIAS)
            #    im.save(filename, 'png')
        except Exception as e:
            print e

    html = _html_feeds(last_feed, user, csrf_token, 'group', feed.group)
    return HttpResponse(html)

@login_required
@ajax_required
def like(request):
    feed_id = request.POST['feed']
    feed = Feed.objects.get(pk=feed_id)
    user = request.user
    like = Activity.objects.filter(activity_type=Activity.LIKE, feed=feed_id,
                                   user=user)
    if like:
        user.profile.unotify_liked(feed)
        like.delete()

    else:
        like = Activity(activity_type=Activity.LIKE, feed=feed_id, user=user)
        like.save()
        user.profile.notify_liked(feed)

    return HttpResponse(feed.calculate_likes())


@login_required
@ajax_required
def comment(request):
    if request.method == 'POST':
        feed_id = request.POST['feed']
        feed = Feed.objects.get(pk=feed_id)
        post = request.POST['post']
        post = post.strip()
        if len(post) > 0:
            post = post[:255]
            user = request.user
            feed.comment(user=user, post=post)
            user.profile.notify_commented(feed)
            user.profile.notify_also_commented(feed)
        return render(request, 'feeds/partial_feed_comments.html',
                      {'feed': feed})

    else:
        feed_id = request.GET.get('feed')
        feed = Feed.objects.get(pk=feed_id)
        return render(request, 'feeds/partial_feed_comments.html',
                      {'feed': feed})


def update(request):
    first_feed = request.GET.get('first_feed')
    last_feed = request.GET.get('last_feed')
    feed_source = request.GET.get('feed_source')
    feed_type = request.GET.get('feed_type')
    feeds = Feed.get_feeds().filter(id__range=(last_feed, first_feed))

    if feed_source != 'all':
        if feed_type == 'profile':
            feeds = feeds.filter(user__id=feed_source)
            feeds = feeds.filter(group='none')
        elif feed_type == 'group':
            feeds = feeds.filter(group=feed_source)
    else:
        feeds = feeds.filter(group='none')
    dump = {}
    for feed in feeds:
        dump[feed.pk] = {'likes': feed.likes, 'comments': feed.comments}
    data = json.dumps(dump)
    return HttpResponse(data, content_type='application/json')


def track_comments(request):
    feed_id = request.GET.get('feed')
    feed = Feed.objects.get(pk=feed_id)
    return render(request, 'feeds/partial_feed_comments.html', {'feed': feed})


@login_required
@ajax_required
def remove(request):
    try:
        feed_id = request.POST.get('feed')
        feed = Feed.objects.get(pk=feed_id)
        if feed.user == request.user:

            filename = django_settings.MEDIA_ROOT + '/post_pictures/' + \
                       str(feed_id) + '.png'

            if os.path.exists(filename):
                os.remove(filename)

            likes = feed.get_likes()
            parent = feed.parent
            for like in likes:
                like.delete()
            feed.delete()
            if parent:
                parent.calculate_comments()

            return HttpResponse()
        else:
            return HttpResponseForbidden()
    except Exception:
        return HttpResponseBadRequest()
