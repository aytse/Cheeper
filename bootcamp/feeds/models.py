from __future__ import unicode_literals

import os.path

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.html import escape
from django.utils.translation import ugettext_lazy as _

import bleach
from bootcamp.activities.models import Activity


@python_2_unicode_compatible
class Feed(models.Model):
    id = models.AutoField(primary_key=True)
    group = models.TextField(max_length=16, default='none')
    user = models.ForeignKey(User)
    date = models.DateTimeField(auto_now_add=True)
    post = models.TextField(max_length=255)
    parent = models.ForeignKey('Feed', null=True, blank=True)
    likes = models.IntegerField(default=0)
    comments = models.IntegerField(default=0)

    class Meta:
        verbose_name = _('Feed')
        verbose_name_plural = _('Feeds')
        ordering = ('-date',)

    def __str__(self):
        return self.post

    @staticmethod
    def get_feeds(from_feed=None):
        if from_feed is not None:
            feeds = Feed.objects.filter(parent=None, id__lte=from_feed)
        else:
            feeds = Feed.objects.filter(parent=None)
        return feeds

    @staticmethod
    def get_feeds_after(feed):
        feeds = Feed.objects.filter(parent=None, id__gt=feed)
        return feeds

    def get_comments(self):
        return Feed.objects.filter(parent=self).order_by('date')

    def calculate_likes(self):
        likes = Activity.objects.filter(activity_type=Activity.LIKE,
                                        feed=self.pk).count()
        self.likes = likes
        self.save()
        return self.likes

    def get_picture(self):
        filename = ''
        picture_url = ''
        no_picture = ''
        try:
            print "ID: %s" % str(self.id)
            for file in os.listdir(settings.MEDIA_ROOT + '/post_pictures/'):
                print "FILE: %s" % file
                if str(self.id) in file:
                    filename = settings.MEDIA_ROOT + '/post_pictures/' + file
                    picture_url = settings.MEDIA_URL + 'post_pictures/' + file

            #filename = settings.MEDIA_ROOT + '/post_pictures/' + \
            #           str(self.id) + '.png'
            #picture_url = settings.MEDIA_URL + 'post_pictures/' + \
            #              str(self.id) + '.png'

            if os.path.isfile(filename):
                return picture_url
            else:
                return no_picture

        except Exception as e:
            print e
            return no_picture

    def get_likes(self):
        likes = Activity.objects.filter(activity_type=Activity.LIKE,
                                        feed=self.pk)
        return likes

    def get_likers(self):
        likes = self.get_likes()
        likers = []
        for like in likes:
            likers.append(like.user)
        return likers

    def calculate_comments(self):
        self.comments = Feed.objects.filter(parent=self).count()
        self.save()
        return self.comments

    def comment(self, user, post):
        feed_comment = Feed(user=user, post=post, parent=self)
        feed_comment.save()
        self.comments = Feed.objects.filter(parent=self).count()
        self.save()
        return feed_comment

    def linkfy_post(self):
        return bleach.linkify(escape(self.post))