import argparse
import requests

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Adds user profile to Cheeper')
    parser.add_argument('Username', help='User\'s username')
    parser.add_argument('Password', help='User\'s passwords')
    parser.add_argument('Email', help='User\'s email')
    parser.add_argument('-l', '--location', help='Location of user')
    parser.add_argument('-u', '--url', help='User\'s url')
    parser.add_argument('-j', '--job', help='User\'s job title')
    parser.add_argument('-p', '--picture', help='User\'s profile picture')
    args = parser.parse_args()

    headers = {'Content-type': 'multipart/form-data'}
    post_fields = {'username': args.Username, 'email': args.Email, 'password': args.Password}

    if args.location:
        post_fields['location'] = args.location
    if args.url:
        post_fields['url'] = args.url
    if args.job:
        post_fields['job_title'] = args.job

    if args.picture:
        r = requests.post('http://localhost:8000/api/add_user/', data=post_fields,
                          files={'profile_picture': open(args.picture, 'rb')})
    else:
        r = requests.post('http://localhost:8000/api/add_user/', data=post_fields)
    print(r.status_code, r.reason)
    print (r.text)

def test_user(user, email, password, location, url, job_title):
    headers = {'Content-type': 'multipart/form-data'}
    post_fields = {'username': user, 'email': email, 'password': password,'location': location, 'url': url, 'job_title': job_title}

    r = requests.post('http://localhost:8000/api/add_user/', data=post_fields, files={'profile_picture': open('pikachu.png', 'rb')})
    print(r.status_code, r.reason)
    print (r.text)

    #test_user('ALLLO_19', 'user_1@narnia.com', 'password', 'Narnia', 'www.narnia.com', 'Plumber')

#r = requests.post(url, files=post_file, data=post_fields)
#r = requests.post(url, data=json.dumps(post_fields))
#print(r.status_code, r.reason)
#print (r.text)