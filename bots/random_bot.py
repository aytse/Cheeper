import json
import random
import requests
import time
import base64

url = 'http://localhost:8000/api/feed/' # Set destination URL here

pending_tweets = []

with open('posts.json') as json_data:
    d =  json.load(json_data)

posts = d['MISC']

while 1:
    post = posts[random.randrange(0,len(posts))]
    user = random.randrange(2, 983)
    group = 'none'
    post_fields = {'user': user, 'post': post, 'group': group}  # Set POST fields here
    headers = { 'Authorization' : 'Basic %s' % base64.b64encode('admin:Rx846nTw')}

    r = requests.post(url, data=json.dumps(post_fields),  auth=('admin', 'Rx846nTw'))
    #r = requests.post(url, data=json.dumps(post_fields), auth=('thenojiko', 'tartans@@1'))
    print(r.status_code, r.reason)
    print (r.text)

    delay = random.randrange(0, 29)
    time.sleep(delay)